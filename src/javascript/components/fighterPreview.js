import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const fighterImg = createFighterImage(fighter);

    const fighterInfoWrapper = createFighterInfo(fighter);

    fighterElement.append(fighterImg, fighterInfoWrapper);
  }

  return fighterElement;
}

export function createFighterInfo(fighter) {
  const { name, health, defense, attack } = fighter;

  const nameWrapper = createElement({
    tagName: 'h2',
  });

  nameWrapper.innerText = name;

  const paramsObj = {
    health: health,
    defense: defense,
    attack: attack,
  };

  const infoElements = [];

  for (const [key, value] of Object.entries(paramsObj)){
    infoElements.push({
      element: createElement({
        tagName: 'p',
        className: 'fighter-preview___text-element'
      }),
      text: [key, value].filter((el) => !!el).join(': '),
    });
  }

  infoElements.forEach((elem) => {
    elem.element.innerText = elem.text;
  });

  const fighterInfoWrapper = createElement({
    tagName: 'div',
    className: 'fighter-preview___text-wrapper'
  });

  fighterInfoWrapper.append(nameWrapper, ...infoElements.map((e) => e.element));

  return fighterInfoWrapper;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
