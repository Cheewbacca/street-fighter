import { controls } from '../../constants/controls';
class Fighter {
  constructor(fighter, position) {
    this.fighter = fighter;
    this.position = position;
    this.health = fighter.health;
    this.currentHealth = fighter.health;
    this.attack = fighter.attack;
    this.defense = fighter.defense;
    this.position = position;
    this.blocked = false;
    this.canCritical = true;
  }

  get fighterHealth() {
    return this.currentHealth;
  }

  get fighterBlock() {
    return this.blocked;
  }

  decreaseHealth(damage, position) {
    console.log(damage);
    if (damage > 0) {
      const healthPercentage = (100 * (this.currentHealth - damage)) / this.health;
      document.getElementById(position + '-fighter-indicator').style.width = `${
        healthPercentage > 0 ? healthPercentage : 0
      }%`;
    }
  }

  receivedDamage(damage) {
    this.currentHealth = this.currentHealth - damage;
    this.decreaseHealth(damage, this.position);
  }

  resetBlock() {
    document.getElementById(this.position + '-fighter-indicator').innerText = '';
    this.blocked = false;
  }

  usesBlock() {
    document.getElementById(this.position + '-fighter-indicator').innerText = 'block';
    this.blocked = true;
  }

  criticalTimer() {
    this.canCritical = false;
    setTimeout(() => (this.canCritical = true), 10000);
  }
}

export const isRightCriticalDamage = (keyArr) => {
  let counter = 0;

  controls.PlayerTwoCriticalHitCombination.forEach((key) => {
    counter = keyArr.has(key) ? counter + 1 : counter;
  });

  if (counter === 3) {
    return true;
  }

  return false;
};

export const isLeftCriticalDamage = (keyArr) => {
  let counter = 0;

  controls.PlayerOneCriticalHitCombination.forEach((key) => {
    counter = keyArr.has(key) ? counter + 1 : counter;
  });

  if (counter === 3) {
    return true;
  }

  return false;
};

export const fightActions = (firstFighter, secondFighter, key) => {
  if (key.has(controls.PlayerOneAttack)) {
    secondFighter.receivedDamage(
      secondFighter.fighterBlock ? getDamage(firstFighter, secondFighter) : getHitPower(firstFighter)
    );
    firstFighter.resetBlock();
  }

  if (key.has(controls.PlayerOneBlock)) {
    firstFighter.usesBlock();
  }

  if (key.has(controls.PlayerTwoAttack)) {
    firstFighter.receivedDamage(
      firstFighter.fighterBlock ? getDamage(secondFighter, firstFighter) : getHitPower(secondFighter)
    );
    secondFighter.resetBlock();
  }

  if (key.has(controls.PlayerTwoBlock)) {
    secondFighter.usesBlock();
  }

  if (isLeftCriticalDamage(key) && firstFighter.canCritical) {
    secondFighter.receivedDamage(getCriticalDamage(firstFighter));
    firstFighter.resetBlock();
    firstFighter.criticalTimer();
  }

  if (isRightCriticalDamage(key) && secondFighter.canCritical) {
    firstFighter.receivedDamage(getCriticalDamage(secondFighter));
    secondFighter.resetBlock();
    secondFighter.criticalTimer();
  }
};

export function needToClearKeysCode(keysCode) {
  if (
    keysCode.has(controls.PlayerOneAttack) ||
    keysCode.has(controls.PlayerOneBlock) ||
    keysCode.has(controls.PlayerTwoAttack) ||
    keysCode.has(controls.PlayerTwoBlock)
  ) {
    return true;
  }

  if (isRightCriticalDamage(keysCode)) {
    return true;
  }

  if (isLeftCriticalDamage(keysCode)) {
    return true;
  }

  return false;
}

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const leftFighter = new Fighter(firstFighter, 'left');
    const rightFighter = new Fighter(secondFighter, 'right');

    const keysCode = new Set();

    document.addEventListener('keydown', (e) => {
      if (!keysCode.has(e.code)) {
        keysCode.add(e.code);
      }

      fightActions(leftFighter, rightFighter, keysCode);

      if (needToClearKeysCode(keysCode)) {
        keysCode.clear();
      }

      if (leftFighter.fighterHealth <= 0) {
        resolve(rightFighter.fighter);
      } else if (rightFighter.fighterHealth <= 0) {
        resolve(leftFighter.fighter);
      }
    });
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export const getCriticalDamage = (attacker) => {
  return attacker.attack * 2;
};

export const randomNumber = () => Math.random() * (2 - 1) + 1;

export function getHitPower(fighter) {
  const criticalHitChance = randomNumber();
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = randomNumber();
  return fighter.defense * dodgeChance;
}
